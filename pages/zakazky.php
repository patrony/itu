<?php
$title = "Zakazky";
?>

<section class="content-header">
    <h1>
        Zakazky
        <small>
            158 zaznamu
        </small>
    </h1>
</section>

<section class="content">
<div class="col-sm-7">
<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne1">
      <h4 class="panel-title">
        <a class="accordion-toggle">
          Strech pan Novak
        </a>
      </h4>
    </div>
    <div id="collapseOne1" class="panel-collapse collapse">
      <div class="panel-body">
        <table class="table table-bordered table-hover table-pointer" id="zakazky-table">
        	 <thead class="center-text">
        	 	<tr>
        	 		<th class="col-sm-2">Datum</th>
        	 		<th class="col-sm-8">Popis</th>
        	 		<th class="col-sm-2">Castka</th>
        	 	</tr>
        	 </tread>
        	 <tbody>
        	 	<tr data-row-id="1">
        	 		<td>11.12.2015</td>
        	 		<td>Nejaky popisek</td>
        	 		<td>+ 7 000 Kc</td>
        	 	</tr>
        	 	<tr data-row-id="2">
        	 		<td>12.12.2015</td>
        	 		<td>Nejaky popisek</td>
        	 		<td>- 4 300 Kc</td>
        	 	</tr>
        	 	<tr data-row-id="3">
        	 		<td>17.12.2015</td>
        	 		<td>Nejaky popisek</td>
        	 		<td>- 2 100 Kc</td>
        	 	</tr>
        	 	<tr data-row-id="4">
        	 		<td colspan="2"><b>Celkem</b></td>
        	 		<td><b>+ 600 Kc</b></td>
        	 	</tr>
        	 </tbody>
     	</table>
     	<button type="button" class="btn btn-success" id="nova_faktura">Priradit fakturu</button>
     	<button type="button" class="btn btn-danger" id="smazat_fakturu">Odebrat fakturu</button>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title"  data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo2">
        <a class="accordion-toggle">
          Kuchyne pani Kropickova
        </a>
      </h4>
    </div>
    <div id="collapseTwo2" class="panel-collapse collapse">
      <div class="panel-body">
        <table class="table table-bordered table-hover table-pointer" id="zakazky2-table">
        	 <thead class="center-text">
        	 	<tr>
        	 		<th class="col-sm-2">Datum</th>
        	 		<th class="col-sm-8">Popis</th>
        	 		<th class="col-sm-2">Castka</th>
        	 	</tr>
        	 </tread>
        	 <tbody>
        	 	<tr data-row-id="1">
        	 		<td>11.12.2015</td>
        	 		<td>Nejaky popisek</td>
        	 		<td>+ 2 000 Kc</td>
        	 	</tr>
        	 	<tr data-row-id="2">
        	 		<td>12.12.2015</td>
        	 		<td>Nejaky popisek</td>
        	 		<td>- 4 300 Kc</td>
        	 	</tr>
        	 	<tr data-row-id="3">
        	 		<td>17.12.2015</td>
        	 		<td>Nejaky popisek</td>
        	 		<td>- 2 100 Kc</td>
        	 	</tr>
        	 	<tr data-row-id="4">
        	 		<td>28.12.2015</td>
        	 		<td>Nejaky popisek</td>
        	 		<td>+ 1 000 Kc</td>
        	 	</tr>
        	 	<tr data-row-id="5">
        	 		<td>5.1.2016</td>
        	 		<td>Nejaky popisek</td>
        	 		<td>- 9 000 Kc</td>
        	 	</tr>
        	 	<tr data-row-id="6">
        	 		<td colspan="2"><b>Celkem</b></td>
        	 		<td><b>- 12 400 Kc</b></td>
        	 	</tr>
        	 </tbody>
     	</table>
     	<button type="button" class="btn btn-success" id="nova_faktura">Priradit fakturu</button>
     	<button type="button" class="btn btn-danger" id="smazat_fakturu">Odebrat fakturu</button>
      </div>
    </div>
  </div>
</div>
</div>

<div class="col-sm-5">
	<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne3">
      <h4 class="panel-title">
        <a class="accordion-toggle">
          Nepriradene faktury
        </a>
      </h4>
    </div>
    <div id="collapseOne3" class="panel-collapse collapse in">
      <div class="panel-body">
        <table class="table table-bordered table-hover table-pointer" id="zakazky-table">
        	 <thead class="center-text">
        	 	<tr>
        	 		<th>Datum</th>
        	 		<th>Popis</th>
        	 		<th>Castka</th>
        	 	</tr>
        	 </tread>
        	 <tbody>
        	 	<tr data-row-id="1">
        	 		<td>11.12.2015</td>
        	 		<td>Nejaky popisek</td>
        	 		<td>+ 7 000 Kc</td>
        	 	</tr>
        	 	<tr data-row-id="2">
        	 		<td>12.12.2015</td>
        	 		<td>Nejaky popisek</td>
        	 		<td>- 4 300 Kc</td>
        	 	</tr>
        	 	<tr data-row-id="3">
        	 		<td>17.12.2015</td>
        	 		<td>Nejaky popisek</td>
        	 		<td>- 2 100 Kc</td>
        	 	</tr>
        	 	<tr data-row-id="4">
        	 		<td colspan="2"><b>Celkem</b></td>
        	 		<td><b>+ 600 Kc</b></td>
        	 	</tr>
        	 </tbody>
     	</table>
      </div>
    </div>
  </div>
</div>

</section>