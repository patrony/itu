<?php
$title = "Peněžní deník";
?>

<section class="content-header">
    <h1>
        Peněžní deník
        <small>
            10 záznamů
        </small>
    </h1>
</section>
<section class="content">
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-pointer" id="diary-table">
            <thead class="center-text">
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Příjmy</th>
                    <th colspan="2">DAŇOVÉ PŘÍJMY</th>
                    <th>Přijaté</th>
                    <th>Výdaje</th>
                    <th colspan="2">DAŇOVÉ VÝDAJE</th>
                    <th>Placené</th>
                    <th colspan="2">Úpravy podle § 23</th>
                </tr>
                <tr>
                    <th>#</th>
                    <th>DATUM</th>
                    <th>DOKLAD</th>
                    <th>OZNAČENÍ TRANSAKCE</th>
                    <th>celkem</th>
                    <th>§ 7</th>
                    <th>§ 8</th>
                    <th>DPH</th>
                    <th>celkem</th>
                    <th>§ 7</th>
                    <th>§ 8</th>
                    <th>DPH</th>
                    <th>Zvyšující ZD</th>
                    <th>Snižující ZD</th>
                </tr>
            </thead>
            <tbody>
                <tr data-row-id="1">
                    <td>1.</td>
                    <td>2.10.</td>
                    <td>BV 023</td>
                    <td>Nákup dl. kostek</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="wsn">12 000 Kč</td>
                    <td class="wsn">10 000 Kč</td>
                    <td></td>
                    <td class="wsn">2 000 Kč</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr data-row-id="2">
                    <td>2.</td>
                    <td>2.10.</td>
                    <td>BV 023</td>
                    <td>Nákup dl. kostek</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>12 000 Kč</td>
                    <td></td>
                    <td class="wsn">10 000 Kč</td>
                    <td>2 000 Kč</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr data-row-id="3">
                    <td>3.</td>
                    <td>2.10.</td>
                    <td>BV 023</td>
                    <td>Nákup dl. kostek</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>12 000 Kč</td>
                    <td>10 000 Kč</td>
                    <td></td>
                    <td>2 000 Kč</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="new">
                    <td colspan="14"><a href="#">Nový záznam</a></td>
                </tr>
            </tbody>
        </table>
        <div class="table-overlay">
            <button type="button" class="btn btn-sm btn-warning edit">Upravit</button>
            <button type="button" class="btn btn-sm btn-danger delete">Smazat</button>
        </div>
    </div>
</section>

<!-- Modal pro potvrzení smazání -->
<div class="modal fade" id="deleteDiaryModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Upozornění</h4>
      </div>
      <div class="modal-body">
          Opravdu chcete smazat tento záznam peněžního deníku?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Ne</button>
        <button type="button" class="btn btn-danger delete-confirm">Smazat</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(function() {
    var $divOverlay = $('.table-overlay');
    var activeRowId = 0;
    $("#diary-table tbody").on("click", "tr[data-row-id]", function(e){ 
        if($(this).is(".editing")) return;
        close = false;
        
        var rowW = $(this).css('width');
        var rowH = $(this).css('height');
        var rowPos = $(this).position();
        var rowTop = rowPos.top;
        
        var posX = e.pageX - $(this).offset().left;
        var btnW = 0;
        
        activeRowId = $(this).attr("data-row-id");
        
        // Zjistí šířku tlačítek uvnitř .table-overlay //
        $divOverlay.show();
        $('*', $divOverlay).each(function(){
            btnW  += $(this).outerWidth(true);
        });
        $divOverlay.hide();
        
        // Vypočte odsazení tlačítek podle pozice kliku //
        var padding = posX - btnW/2;
        if(padding < 0){
            padding = 0;
        }
        else if((padding + btnW) > parseInt(rowW)){
            padding = parseInt(rowW) - btnW;
        }
        
        // nastaví .table-overlay aby překrýval řádek //
        $divOverlay.css({
            position: 'absolute',
            top: rowTop,
            left: '0px',
            paddingLeft: padding + 'px',
            width: rowW,
            height: rowH
        });
        
        // Zobrazí table-overlay //
        $divOverlay.slideDown("fast", function(){
            $(".table-overlay").css('display', 'flex');
        });
    });
    
    // skryje .table-overlay //
    var close = false;
    $(".table-overlay").on("mouseleave tap", function(e){
        close = true;
        setTimeout(function(){
            if(close) $divOverlay.hide();
        }, 300); 
    });
    
    // přeruší skrytí table-overlay pokud na něj stihne včas najet //
    $(".table-overlay").on("mouseenter", function(e){
        close = false;
    });
    
    // Upravit řádek //
    $(".table-overlay .edit").on("click", function(e){ 
        var $activeRow = $("#diary-table tr[data-row-id="+activeRowId+"]");
        $divOverlay.hide();
        
        $activeRow.addClass("editing");
        $activeRow.children("td").each(function(index, el){
            if(!index) return true;
            var text = $(this).text();
            var width = $(this).innerWidth() - 6;
            var $input = $("<input type='text'>")
                    .val(text)
                    .width(width)
                    .data("original", text);
            $(this).html($input);
        });
        
        var $saveBtn = $("<button type='button'>Uložit</button>")
                .addClass("btn btn-success btn-sm mr5")
                .click(saveChanges);
        
        var $cancelBtn = $("<button type='button'>Zrušit</button>")
                .addClass("btn btn-default btn-sm")
                .click(cancelChanges);
        
        var $newCol = $("<td colspan='14'></td>").append($saveBtn)
                .append($cancelBtn);
        
        var $newRow = $("<tr></tr>").append($newCol);
        $activeRow.after($newRow);
    });
    
    // Mazání řádků //
    $(".table-overlay .delete").on("click", function(e){ 
        $divOverlay.hide();
        $('#deleteDiaryModal').modal('show');
    });
    
    $(".delete-confirm").on("click", function(e){ 
        var $activeRow = $("#diary-table tr[data-row-id="+activeRowId+"]");
        $('#deleteDiaryModal').modal('hide');
        $activeRow.remove();
    });
    
    // Nový řádek //
    $("#diary-table .new").on("click", function(e){ 
        
        var count = $("#diary-table tbody tr[data-row-id]").length + 1;
        
        var $newRow = $("<tr></tr>")
                .append("<td>"+count+".</td>")
                .attr("data-row-id", count)
                .addClass("editing");
        
        for(var i = 0; i < 13; i++){
            var $input = $("<input type='text'>");
            var $newCol = $("<td></td>").append($input);
            $newRow.append($newCol);
        }
        
        var $saveBtn = $("<button type='button'>Vložit</button>")
                .addClass("btn btn-primary btn-sm mr5")
                .click(saveChanges);
        
        var $cancelBtn = $("<button type='button'>Zrušit</button>")
                .addClass("btn btn-default btn-sm")
                .click(cancelNew);
        
        var $newCol = $("<td colspan='14'></td>").append($saveBtn)
                .append($cancelBtn);
        
        var $controlRow = $("<tr></tr>").append($newCol);
        
        $(this).before($newRow);
        $(this).before($controlRow);
    });
    
    var saveChanges = function(){
        var $activeRow = $(this).closest("tr").prev();
        
        $activeRow.removeClass("editing");
        $activeRow.children("td").each(function(index, el){
            if(!index) return true;
            var text = $("input",this).val();
            $(this).text(text);
        });
        
        $(this).closest("tr").remove();
    };
    
    var cancelChanges = function(){
        var $activeRow = $(this).closest("tr").prev();
        
        $activeRow.removeClass("editing");
        $activeRow.children("td").each(function(index, el){
            if(!index) return true;
            var text = $("input",this).data("original");
            $(this).text(text);
        });
        
        $(this).closest("tr").remove();
    };
    
    var cancelNew = function(){
        var $activeRow = $(this).closest("tr").prev();
        
        $activeRow.remove();        
        $(this).closest("tr").remove();
    };
});
</script>