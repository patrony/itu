<?php
$title = "Todo list";
?>

<section class="content-header">
    <h1>
        Todo list
        <small>
            25 záznamů
        </small>
    </h1>
</section>

<section class="content">
 <div class="col-sm-8">
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-pointer" id="todo-table">
            <thead class="center-text">
                <tr>
                	<th class="col-sm-1">#</th>
                    <th class="col-sm-1">Datum</th>
                    <th class="col-sm-6">Popis</th>
                </tr>
            </thead>
             <tbody>
                <tr todo-row-id="1">
                	<td>1.</td>
                    <td>12.12.2015</td>
                    <td>Popisok k tomuto datumu</td>
                </tr>
                <tr todo-row-id="2">
                	<td>2.</td>
                    <td>12.12.2015</td>
                    <td>Popisok k tomuto datumu</td>
                </tr>
                <tr todo-row-id="3">
                	<td>3.</td>
                    <td>13.12.2015</td>
                    <td>Popisok k tomuto datumu</td>
                </tr>
                <tr class="new">
                    <td colspan="3"><a href="#">Nový záznam</a></td>
                </tr>
            </tbody>
        </table>
        <div class="table-overlay">
            <button type="button" class="btn btn-sm btn-warning edit">Upravit</button>
            <button type="button" class="btn btn-sm btn-danger delete">Smazat</button>
        </div>


        <section class="content-header">
        <h1>
            Poznámky
            <small>
                25 záznamů
            </small>
        </h1>
        </section>


        <table class="table table-bordered table-hover table-pointer" id="notes-table">
            <thead class="center-text">
                <tr>
                    <th class="col-sm-1">#</th>
                    <th class="col-sm-6">Popis</th>
                </tr>
            </thead>
             <tbody>
                <tr notes-row-id="1">
                    <td>1.</td>
                    <td>Poznámka jedna</td>
                </tr>
                <tr notes-row-id="2">
                    <td>2.</td>
                    <td>Poznámka dvě</td>
                </tr>
                <tr notes-row-id="3">
                    <td>3.</td>
                    <td>Poznámka tři</td>
                </tr>
                <tr class="new">
                    <td colspan="2"><a href="#">Nový záznam</a></td>
                </tr>
            </tbody>
        </table>
                <div class="table-overlay">
            <button type="button" class="btn btn-sm btn-warning edit">Upravit</button>
            <button type="button" class="btn btn-sm btn-danger delete">Smazat</button>
        </div>

    </div>
</div>
<div class="col-sm-4">
    <!-- calc -->
    <div class="col-sm-4">
    	Kalendar
   	</div>
</div>
</section>


<!-- Modal pro potvrzení smazání -->
<div class="modal fade" id="deleteDiaryModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Upozornění</h4>
      </div>
      <div class="modal-body">
          Opravdu chcete smazat tento záznam peněžního deníku?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Ne</button>
        <button type="button" class="btn btn-danger delete-confirm">Smazat</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(function() {
    var $divOverlay = $('.table-overlay');
    var activeRowId = 0;
    $("#todo-table tbody").on("click", "tr[todo-row-id]", function(e){ 
        if($(this).is(".editing")) return;
        close = false;
        
        var rowW = $(this).css('width');
        var rowH = $(this).css('height');
        var rowPos = $(this).position();
        var rowTop = rowPos.top;
        
        var posX = e.pageX - $(this).offset().left;
        var btnW = 0;
        
        activeRowId = $(this).attr("todo-row-id");
        
        // Zjistí šířku tlačítek uvnitř .table-overlay //
        $divOverlay.show();
        $('*', $divOverlay).each(function(){
            btnW  += $(this).outerWidth(true);
        });
        $divOverlay.hide();
        
        // Vypočte odsazení tlačítek podle pozice kliku //
        var padding = posX - btnW/2;
        if(padding < 0){
            padding = 0;
        }
        else if((padding + btnW) > parseInt(rowW)){
            padding = parseInt(rowW) - btnW;
        }
        
        // nastaví .table-overlay aby překrýval řádek //
        $divOverlay.css({
            position: 'absolute',
            top: rowTop,
            left: '0px',
            paddingLeft: padding + 'px',
            width: rowW,
            height: rowH
        });
        
        // Zobrazí table-overlay //
        $divOverlay.slideDown("fast", function(){
            $(".table-overlay").css('display', 'flex');
        });
    });
    
    // skryje .table-overlay //
    var close = false;
    $(".table-overlay").on("mouseleave tap", function(e){
        close = true;
        setTimeout(function(){
            if(close) $divOverlay.hide();
        }, 300); 
    });
    
    // přeruší skrytí table-overlay pokud na něj stihne včas najet //
    $(".table-overlay").on("mouseenter", function(e){
        close = false;
    });
    
    // Upravit řádek //
    $(".table-overlay .edit").on("click", function(e){ 
        var $activeRow = $("#todo-table tr[todo-row-id="+activeRowId+"]");
        $divOverlay.hide();
        
        $activeRow.addClass("editing");
        $activeRow.children("td").each(function(index, el){
            if(!index) return true;
            var text = $(this).text();
            var width = $(this).innerWidth() - 6;
            var $input = $("<input type='text'>")
                    .val(text)
                    .width(width)
                    .data("original", text);
            $(this).html($input);
        });
        
        var $saveBtn = $("<button type='button'>Uložit</button>")
                .addClass("btn btn-success btn-sm mr5")
                .click(saveChanges);
        
        var $cancelBtn = $("<button type='button'>Zrušit</button>")
                .addClass("btn btn-default btn-sm")
                .click(cancelChanges);
        
        var $newCol = $("<td colspan='3'></td>").append($saveBtn)
                .append($cancelBtn);
        
        var $newRow = $("<tr></tr>").append($newCol);
        $activeRow.after($newRow);
    });
    
    // Mazání řádků //
    $(".table-overlay .delete").on("click", function(e){ 
        $divOverlay.hide();
        $('#deleteDiaryModal').modal('show');
    });
    
    $(".delete-confirm").on("click", function(e){ 
        var $activeRow = $("#todo-table tr[todo-row-id="+activeRowId+"]");
        $('#deleteDiaryModal').modal('hide');
        $activeRow.remove();
    });
    
    // Nový řádek //
    $("#todo-table .new").on("click", function(e){ 
        
        var count = $("#todo-table tbody tr[todo-row-id]").length + 1;
        
        var $newRow = $("<tr></tr>")
                .append("<td>"+count+".</td>")
                .attr("todo-row-id", count)
                .addClass("editing");
        
        for(var i = 0; i < 2; i++){
            var $input = $("<input type='text'>");
            var $newCol = $("<td></td>").append($input);
            $newRow.append($newCol);
        }
        
        var $saveBtn = $("<button type='button'>Vložit</button>")
                .addClass("btn btn-primary btn-sm mr5")
                .click(saveChanges);
        
        var $cancelBtn = $("<button type='button'>Zrušit</button>")
                .addClass("btn btn-default btn-sm")
                .click(cancelNew);
        
        var $newCol = $("<td colspan='3'></td>").append($saveBtn)
                .append($cancelBtn);
        
        var $controlRow = $("<tr></tr>").append($newCol);
        
        $(this).before($newRow);
        $(this).before($controlRow);
    });
    
    var saveChanges = function(){
        var $activeRow = $(this).closest("tr").prev();
        
        $activeRow.removeClass("editing");
        $activeRow.children("td").each(function(index, el){
            if(!index) return true;
            var text = $("input",this).val();
            $(this).text(text);
        });
        
        $(this).closest("tr").remove();
    };
    
    var cancelChanges = function(){
        var $activeRow = $(this).closest("tr").prev();
        
        $activeRow.removeClass("editing");
        $activeRow.children("td").each(function(index, el){
            if(!index) return true;
            var text = $("input",this).data("original");
            $(this).text(text);
        });
        
        $(this).closest("tr").remove();
    };
    
    var cancelNew = function(){
        var $activeRow = $(this).closest("tr").prev();
        
        $activeRow.remove();        
        $(this).closest("tr").remove();
    };
});

$(function() {
    var $divOverlay = $('.table-overlay');
    var activeRowId = 0;
    $("#notes-table tbody").on("click", "tr[notes-row-id]", function(e){ 
        if($(this).is(".editing")) return;
        close = false;
        
        var rowW = $(this).css('width');
        var rowH = $(this).css('height');
        var rowPos = $(this).position();
        var rowTop = rowPos.top;
        
        var posX = e.pageX - $(this).offset().left;
        var btnW = 0;
        
        activeRowId = $(this).attr("notes-row-id");
        
        // Zjistí šířku tlačítek uvnitř .table-overlay //
        $divOverlay.show();
        $('*', $divOverlay).each(function(){
            btnW  += $(this).outerWidth(true);
        });
        $divOverlay.hide();
        
        // Vypočte odsazení tlačítek podle pozice kliku //
        var padding = posX - btnW/2;
        if(padding < 0){
            padding = 0;
        }
        else if((padding + btnW) > parseInt(rowW)){
            padding = parseInt(rowW) - btnW;
        }
        
        // nastaví .table-overlay aby překrýval řádek //
        $divOverlay.css({
            position: 'absolute',
            top: rowTop,
            left: '0px',
            paddingLeft: padding + 'px',
            width: rowW,
            height: rowH
        });
        
        // Zobrazí table-overlay //
        $divOverlay.slideDown("fast", function(){
            $(".table-overlay").css('display', 'flex');
        });
    });
    
    // skryje .table-overlay //
    var close = false;
    $(".table-overlay").on("mouseleave tap", function(e){
        close = true;
        setTimeout(function(){
            if(close) $divOverlay.hide();
        }, 300); 
    });
    
    // přeruší skrytí table-overlay pokud na něj stihne včas najet //
    $(".table-overlay").on("mouseenter", function(e){
        close = false;
    });
    
    // Upravit řádek //
    $(".table-overlay .edit").on("click", function(e){ 
        var $activeRow = $("#notes-table tr[notes-row-id="+activeRowId+"]");
        $divOverlay.hide();
        
        $activeRow.addClass("editing");
        $activeRow.children("td").each(function(index, el){
            if(!index) return true;
            var text = $(this).text();
            var width = $(this).innerWidth() - 6;
            var $input = $("<input type='text'>")
                    .val(text)
                    .width(width)
                    .data("original", text);
            $(this).html($input);
        });
        
        var $saveBtn = $("<button type='button'>Uložit</button>")
                .addClass("btn btn-success btn-sm mr5")
                .click(saveChanges);
        
        var $cancelBtn = $("<button type='button'>Zrušit</button>")
                .addClass("btn btn-default btn-sm")
                .click(cancelChanges);
        
        var $newCol = $("<td colspan='3'></td>").append($saveBtn)
                .append($cancelBtn);
        
        var $newRow = $("<tr></tr>").append($newCol);
        $activeRow.after($newRow);
    });
    
    // Mazání řádků //
    $(".table-overlay .delete").on("click", function(e){ 
        $divOverlay.hide();
        $('#deleteDiaryModal').modal('show');
    });
    
    $(".delete-confirm").on("click", function(e){ 
        var $activeRow = $("#notes-table tr[notes-row-id="+activeRowId+"]");
        $('#deleteDiaryModal').modal('hide');
        $activeRow.remove();
    });
    
    // Nový řádek //
    $("#notes-table .new").on("click", function(e){ 
        
        var count = $("#notes-table tbody tr[notes-row-id]").length + 1;
        
        var $newRow = $("<tr></tr>")
                .append("<td>"+count+".</td>")
                .attr("notes-row-id", count)
                .addClass("editing");
        
        for(var i = 0; i < 1; i++){
            var $input = $("<input type='text'>");
            var $newCol = $("<td></td>").append($input);
            $newRow.append($newCol);
        }
        
        var $saveBtn = $("<button type='button'>Vložit</button>")
                .addClass("btn btn-primary btn-sm mr5")
                .click(saveChanges);
        
        var $cancelBtn = $("<button type='button'>Zrušit</button>")
                .addClass("btn btn-default btn-sm")
                .click(cancelNew);
        
        var $newCol = $("<td colspan='2'></td>").append($saveBtn)
                .append($cancelBtn);
        
        var $controlRow = $("<tr></tr>").append($newCol);
        
        $(this).before($newRow);
        $(this).before($controlRow);
    });
    
    var saveChanges = function(){
        var $activeRow = $(this).closest("tr").prev();
        
        $activeRow.removeClass("editing");
        $activeRow.children("td").each(function(index, el){
            if(!index) return true;
            var text = $("input",this).val();
            $(this).text(text);
        });
        
        $(this).closest("tr").remove();
    };
    
    var cancelChanges = function(){
        var $activeRow = $(this).closest("tr").prev();
        
        $activeRow.removeClass("editing");
        $activeRow.children("td").each(function(index, el){
            if(!index) return true;
            var text = $("input",this).data("original");
            $(this).text(text);
        });
        
        $(this).closest("tr").remove();
    };
    
    var cancelNew = function(){
        var $activeRow = $(this).closest("tr").prev();
        
        $activeRow.remove();        
        $(this).closest("tr").remove();
    };
});
</script>