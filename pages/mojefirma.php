<?php
$title = "Moje firma";
?>

<section class="content-header">
    <h1>
    	Moje Firma
    </h1>
</section>

<section class="content">
<div class="panel panel-default">
  	<div class="panel-heading">
  		Nazov Firmy: Pro-gaming s.r.o
  	</div>
  		<ul class="list-group">
            <li class="list-group-item list-group-item-default">Adresa: Kolejni</li>
            <li class="list-group-item list-group-item-default">Tel. cislo: +420 834 864 447</li>
            <li class="list-group-item list-group-item-default">PCS: 612 00</li>
            <li class="list-group-item list-group-item-default">IC: 7543156987</li>
            <li class="list-group-item list-group-item-default">DIC: 1479</li>
            <li class="list-group-item list-group-item-default">Bankove spojenie: 975412316548/988</li>
  		</ul>
	</div>
</section>