<?php
$title = "Kontakty";
?>

<section class="content-header">
    <h1>
        Kontakty
        <small>
            3 záznamy
        </small>
    </h1>
</section>


<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne1">
      <h4 class="panel-title">
        <a class="accordion-toggle">
          Robert Adam
        </a>
      </h4>
    </div>
    <div id="collapseOne1" class="panel-collapse collapse">
      <div class="panel-body">
        <p>
          <ul class="list-group">
            <li class="list-group-item list-group-item-info">Nazov Firmy: Pro-gaming s.r.o</li>
            <li class="list-group-item list-group-item-default">Adresa: Kolejni</li>
            <li class="list-group-item list-group-item-default">Tel. cislo: +420 834 864 447</li>
            <li class="list-group-item list-group-item-default">PCS: 612 00</li>
            <li class="list-group-item list-group-item-default">IC: 7543156987</li>
            <li class="list-group-item list-group-item-default">DIC: 1479</li>
            <li class="list-group-item list-group-item-default">Bankove spojenie: 975412316548/988</li>
          </ul>
        </p>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title"  data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo2">
        <a class="accordion-toggle">
          Patrik Vaclavek
        </a>
      </h4>
    </div>
    <div id="collapseTwo2" class="panel-collapse collapse">
      <div class="panel-body">
        <ul class="list-group">
            <li class="list-group-item list-group-item-info">Nazov Firmy: </li>
            <li class="list-group-item list-group-item-default">Adresa: </li>
            <li class="list-group-item list-group-item-default">Tel. cislo: </li>
            <li class="list-group-item list-group-item-default">PCS: </li>
            <li class="list-group-item list-group-item-default">IC: </li>
            <li class="list-group-item list-group-item-default">DIC: </li>
            <li class="list-group-item list-group-item-default">Bankove spojenie: </li>
          </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" data-target="#collapseThree3">
        <a class="accordion-toggle">
          Jirka Pomikalek
        </a>
      </h4>
    </div>
    <div id="collapseThree3" class="panel-collapse collapse">
      <div class="panel-body">
        <ul class="list-group">
            <li class="list-group-item list-group-item-info">Nazov Firmy: </li>
            <li class="list-group-item list-group-item-default">Adresa: </li>
            <li class="list-group-item list-group-item-default">Tel. cislo: </li>
            <li class="list-group-item list-group-item-default">PCS: </li>
            <li class="list-group-item list-group-item-default">IC: </li>
            <li class="list-group-item list-group-item-default">DIC: </li>
            <li class="list-group-item list-group-item-default">Bankove spojenie: </li>
          </ul>
      </div>
    </div>
  </div>
</div>






<div class="panel-group" id="accordion">
<div class="col-sm-4">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
        <a class="accordion-toggle">
          Vytvorit novy kontakt
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
      <div class="panel-body">
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon2">Meno Kontaktu</span>
          <input type="text" class="form-control" placeholder="Zadajte meno kontaktu" aria-describedby="sizing-addon1">
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon2">Nazov firmy</span>
          <input type="text" class="form-control" placeholder="Zadajte nazov firmy" aria-describedby="sizing-addon1">
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon2">Adresa</span>
          <input type="text" class="form-control" placeholder="Zadajte adresu trvaleho bydliska" aria-describedby="sizing-addon1">
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon2">Tel. cislo</span>
          <input type="text" class="form-control" placeholder="Zadajte tel. cislo" aria-describedby="sizing-addon1">
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon2">PCS</span>
          <input type="text" class="form-control" placeholder="Zadajte postove smerove cislo" aria-describedby="sizing-addon1">
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon2">IC</span>
          <input type="text" class="form-control" placeholder="Zadajte IC" aria-describedby="sizing-addon1">
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon2">DIC</span>
          <input type="text" class="form-control" placeholder="Zadajte DIC" aria-describedby="sizing-addon1">
        </div>
        <br>
        <div class="input-group">
          <span class="input-group-addon" id="sizing-addon2">Bankovni spojeni</span>
          <input type="text" class="form-control" placeholder="Zadajte cislo uctu" aria-describedby="sizing-addon1">
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-sm-4">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" data-target="#collapseThree">
        <a class="accordion-toggle">
          Upravit kontakt
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
        <div class="dropdown col-sm-9">
          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Vyberte meno kontaktu
          <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="#">Robert Adam</a></li>
            <li><a href="#">Patrik Vaclavek</a></li>
            <li><a href="#">Jirka Pomikalek</a></li>
          </ul>
        </div>
        <button type="button" class="btn btn-success col-sm-3">Potvrdit</button>
      </div>
    </div>
  </div>
</div>


<div class="col-sm-4">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo">
        <a class="accordion-toggle">
          Zmazat kontakt
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <div class="dropdown col-sm-9">
          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          Vyberte meno kontaktu
          <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="#">Robert Adam</a></li>
            <li><a href="#">Patrik Vaclavek</a></li>
            <li><a href="#">Jirka Pomikalek</a></li>
          </ul>
        </div>
        <button type="button" class="btn btn-success col-sm-3">Potvrdit</button>
      </div>
    </div>
  </div>
</div>
</div>