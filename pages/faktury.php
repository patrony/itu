<?php
$title = "Faktury";
?>

<section class="content-header">
    <h1>
        Faktury
        <small>
            2 faktury
        </small>
    </h1>
</section>
<section class="content">
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-pointer" id="factures-table">
            <thead class="center-text">
                <tr>
                    <th></th>
                    <th class="col-xs-2">Číslo faktury</th>
                    <th class="col-xs-4">Název</th>
                    <th class="col-xs-2">Odběratel</th>
                    <th class="col-xs-2">Datum splatnosti</th>
                    <th class="col-xs-2">Částka</th>
                    <th>Uhrazeno</th>
                </tr>
            </thead>
            <tbody>
                <tr data-row-id="1">
                    <td><i class="fa fa-plus fa-lg"></i></td>
                    <td>F0001</td>
                    <td>Faktura č. 1</td>
                    <td>Petr Novák</td>
                    <td>25.12.2015</td>
                    <td>2 258 Kč</td>
                    <td class="text-green"><i class="fa fa-check-circle-o fa-2x"></i></td>
                </tr>
                <tr>
                    <td colspan="7">
                        <div class="col-xs-12 form-control-static">
                            <button type='button' class="btn btn-default btn-sm mr5 pay">Stornovat</button>
                            <button type='button' class="btn btn-warning btn-sm mr5 edit">Upravit</button>
                            <button type='button' class="btn btn-danger btn-sm delete">Smazat</button>
                        </div>
                    </td>
                </tr>
                <tr data-row-id="1">
                    <td><i class="fa fa-plus fa-lg"></i></td>
                    <td>F0002</td>
                    <td>Faktura č. 2</td>
                    <td>Tomáš Novotný</td>
                    <td>25.12.2015</td>
                    <td>21 000,50 Kč</td>
                    <td class="text-red"><i class="fa fa-times fa-2x"></i></td>
                </tr>
                <tr>
                    <td colspan="7">
                        <div class="col-xs-12 form-control-static">
                            <button type='button' class="btn btn-default btn-sm mr5 pay">Uhradit</button>
                            <button type='button' class="btn btn-warning btn-sm mr5 edit">Upravit</button>
                            <button type='button' class="btn btn-danger btn-sm delete">Smazat</button>
                        </div>
                    </td>
                </tr>
                <tr class="new">
                    <td colspan="14"><a href="?s=faktura">Nová faktura</a></td>
                </tr>
            </tbody>
        </table>
        <div class="table-overlay">
            <button type="button" class="btn btn-sm btn-warning edit">Upravit</button>
            <button type="button" class="btn btn-sm btn-danger delete">Smazat</button>
        </div>
    </div>
</section>

<!-- Modal pro potvrzení smazání -->
<div class="modal fade" id="deleteFactureModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Upozornění</h4>
      </div>
      <div class="modal-body">
          Opravdu chcete smazat tuto fakturu?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Ne</button>
        <button type="button" class="btn btn-danger delete-confirm">Smazat</button>
      </div>
    </div>
  </div>
</div>
<script src="./js/faktury.js"></script>