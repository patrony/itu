<?php
$title = "Dashboard";
?>

<section class="content-header">
    <h1>
        Přehled
    </h1>
</section>

<section class="content">
<div class="col-sm-6">

	<!-- TODO list -->
	<div class="panel panel-warning">
  		<!-- Default panel contents -->
      <!-- <a href="http://www.stud.fit.vutbr.cz/~xadamr01/ITU/?s=todo"> -->
  		<div class="panel-heading" onclick="location.href='http://www.stud.fit.vutbr.cz/~xadamr01/ITU/?s=todo'" style="cursor: pointer">
        TODO list
        <!--<a href="http://www.stud.fit.vutbr.cz/~xadamr01/ITU/?s=todo" class="btn btn-warning pull-right" role="button">MORE</a>-->
      </div>
      

  		<!-- Table -->
  		<table class="table">
    		<tr>
    			<th class="col-sm-3">Datum</th>
    			<th class="col-sm-9">Popis</th>
    		</tr>
    		<tr>
    			<td>8.12.2015</td>
    			<td>Makat na ITU</td>
    		</tr>
    		<tr>
    			<td>9.12.2015</td>
    			<td>Podivat se do ISA</td>
    		</tr>
    		<tr>
    			<td>13.12.2015</td>
    			<td>Nedelni Chill</td>
    		</tr>
  		</table>
	</div>

	<!-- Zakazky -->
	<div class="panel panel-primary">
  		<!-- Default panel contents -->
  		<div class="panel-heading" onclick="location.href='http://www.stud.fit.vutbr.cz/~xadamr01/ITU/?s=zakazky'" style="cursor: pointer">Zakázky</div>

  		<!-- Table -->
  		<table class="table">
    		<tr>
    			<th class="col-sm-9">Názov</th>
    			<th class="col-sm-3">Částka</th>
    		</tr>
    		<tr>
    			<td>Stipendium</td>
    			<td>+ 14 250 Kc</td>
    		</tr>
    		<tr>
    			<td>Kolej</td>
    			<td>- 4 110 Kc</td>
    		</tr>
    		<tr>
    			<td>Mafianske vydelky</td>
    			<td>+ 2 040 Kc</td>
    		</tr>
  		</table>
	</div>
</div>

<div class="col-sm-6">

	<!-- Nezaplacene faktury -->
	<div class="panel panel-danger">
  		<!-- Default panel contents -->
  		<div class="panel-heading" onclick="location.href='http://www.stud.fit.vutbr.cz/~xadamr01/ITU/?s=faktury'" style="cursor: pointer">Nezaplacené faktury</div>
  		<!-- Table -->
  		<table class="table">
    		<tr>
    			<th class="col-sm-3">Datum</th>
    			<th class="col-sm-6">Popis</th>
    			<th class="col-sm-3">Částka</th>
    		</tr>
    		<tr>
    			<td>12.12.2015</td>
    			<td>Rohliky</td>
    			<td>5.0 Kc</td>
    		</tr>
    		<tr>
    			<td>18.12.2015</td>
    			<td>Salam</td>
    			<td>29.30 Kc</td>
    		</tr>
    		<tr>
    			<td>22.12.2015</td>
    			<td>Sunka</td>
    			<td>27.50 Kc</td>
    		</tr>
  		</table>
	</div>

	<!-- Poznamky -->
	<div class="panel panel-success">
  		<div class="panel-heading" onclick="location.href='http://www.stud.fit.vutbr.cz/~xadamr01/ITU/?s=todo'" style="cursor: pointer">
  			Poznamky
  		</div>
  		<!--<div class="panel-body"></div>-->
  		<ul class="list-group">
    		<li class="list-group-item"><kbd>1</kbd> lfasjkh hfsjadkh kljsfd</li>
    		<li class="list-group-item"><kbd>2</kbd> werj iojtw ekm kjtr</li>
    		<li class="list-group-item">p jiog un wqmknj vge</li>
    		<li class="list-group-item">rfehuih ohjurios pomrelkn</li>
  		</ul>
	</div>
</div>

<div class="col-sm-8">
	<!-- Graf -->
	<div class="panel panel-info">
		<div class="panel-heading">
  			Graf
  	</div>
  		<div class="panel-body">
    		<img src="http://www.stud.fit.vutbr.cz/~xadamr01/ITU/images/graf_obrat.png" alt="Obrat">
      </div>
	</div>
</div>

<div class="col-sm-4">
	<!-- Statistiky -->
	<div class="panel panel-info">
		<div class="panel-heading">
  			Statistiky
  		</div>
  		<table class="table" id="dash-table6">
        <tbody>
    		<tr>
    			<td><b>Zůstatek za minule období</b></td>
    			<td>+ 50 000 Kc</font></td>
    		</tr>
    		<tr>
    			<td><b>Příjmy</b></td>
    			<td>+ 32 000 Kc</td>
    		</tr>
    		<tr>
    			<td><b>Výdaje</b></td>
    			<td>- 25 000 Kc</td>
    		</tr>
    		<tr>
    			<td><b>Hospodársky výsledek</b></td>
    			<td>- 7 000 Kc</td>
    		</tr>
    		<tr>
    			<td><b>Celkem</b></td>
    			<td>+ 7 000 Kc</td>
    		</tr>
      </tbody>
    	</table>
  	<div>
</div>

</section>
