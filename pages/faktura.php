<?php
$title = "Faktury";
?>

<section class="content-header">
    <h1>
        Nová faktura
    </h1>
</section>
<section class="content">
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="form">
            <div class="row form-horizontal">
                <div class="col-md-6 ">
                    <div class="panel panel-default">
                        <div class="panel-heading">Dodavatel</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Jméno firmy</label>                            
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                        <span class="input-group-btn btn-contact">
                                            <button class="btn btn-default" type="button"><i class="fa fa-user"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Adresa, č. popisné</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Město</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">PSČ</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">IČ</label>
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i> ARES</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">DIČ</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="panel panel-default">
                        <div class="panel-heading">Odběratel</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-5 control-label btn-contact">Jméno firmy</label>                            
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                        <span class="input-group-btn btn-contact">
                                            <button class="btn btn-default" type="button"><i class="fa fa-user"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Adresa, č. popisné</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Město</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">PSČ</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">IČ</label>
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-search"></i> ARES</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">DIČ</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="panel panel-default">
                        <div class="panel-heading">Platba</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Částka</label>                            
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <span class="input-group-btn btn-calculator">
                                            <button class="btn btn-default" type="button"><i class="fa fa-calculator"></i></button>
                                        </span>
                                        <input type="number" class="form-control" min="0" value="0">
                                        <span class="input-group-addon">Kč</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Variabilní symbol</label>                            
                                <div class="col-sm-5">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Konstantní symbol</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Specifický symbol</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="panel panel-default">
                        <div class="panel-heading">Faktura</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Název faktury</label>                            
                                <div class="col-sm-6">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Číslo faktury</label>                            
                                <div class="col-sm-3">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Datum vystavení</label>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" value="" class="form-control calendar" placeholder="d.m.Y">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Datum splatnosti</label>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" value="" class="form-control calendar" placeholder="d.m.Y">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="info-box">
                        <a href="#" class="btn btn-primary">Vytvořit</a>
                        <a href="#" class="btn btn-default">Zrušit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal pro potvrzení smazání -->
<div class="modal fade" id="contactModal" tabindex="-1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Výběr z kontaktů</h4>
      </div>
      <div class="modal-body">
          <table class="table table-bordered table-hover table-pointer" id="contactTable">
              <thead>
                  <tr>
                      <th>Jméno</th>
                  </tr>
              </thead>
              <tbody>
                  <tr data-row-id="1">
                      <td>Robert Adam</td>
                  </tr>
                  <tr data-row-id="2">
                      <td>Patrik Václavek</td>
                  </tr>
                  <tr data-row-id="3">
                      <td>Jirka Pomikalek</td>
                  </tr>
              </tbody>
          </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
        <button type="button" class="btn btn-primary">Vybrat</button>
      </div>
    </div>
  </div>
</div>
<script src="./js/faktura.js"></script>