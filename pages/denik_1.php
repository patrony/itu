<?php
$title = "Peněžní deník";
?>

<section class="content-header">
    <h1>
        Peněžní deník
        <small>
            10 záznamů
        </small>
    </h1>
</section>
<section class="content">
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-pointer" id="diary-table">
            <thead class="center-text">
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Příjmy</th>
                    <th colspan="2">DAŇOVÉ PŘÍJMY</th>
                    <th>Přijaté</th>
                    <th>Výdaje</th>
                    <th colspan="2">DAŇOVÉ VÝDAJE</th>
                    <th>Placené</th>
                    <th colspan="2">Úpravy podle § 23</th>
                </tr>
                <tr>
                    <th>#</th>
                    <th>DATUM</th>
                    <th>DOKLAD</th>
                    <th>OZNAČENÍ TRANSAKCE</th>
                    <th>celkem</th>
                    <th>§ 7</th>
                    <th>§ 8</th>
                    <th>DPH</th>
                    <th>celkem</th>
                    <th>§ 7</th>
                    <th>§ 8</th>
                    <th>DPH</th>
                    <th>Zvyšující ZD</th>
                    <th>Snižující ZD</th>
                </tr>
            </thead>
            <tbody>
                <tr data-row-id="1">
                    <td>1.</td>
                    <td>2.10.2015</td>
                    <td>BV 023</td>
                    <td>Nákup dl. kostek</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="wsn">12 000 Kč</td>
                    <td class="wsn">10 000 Kč</td>
                    <td></td>
                    <td class="wsn">2 000 Kč</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr data-row-id="2">
                    <td>2.</td>
                    <td>3.10.2015</td>
                    <td>BV 023</td>
                    <td>Nákup dl. kostek</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>1428,571 Kč</td>
                    <td></td>
                    <td class="wsn">10 000 Kč</td>
                    <td>2 000 Kč</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr data-row-id="3">
                    <td>3.</td>
                    <td>6.10.2015</td>
                    <td>BV 023</td>
                    <td>Nákup dl. kostek</td>
                    <td>0</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>12 000 Kč</td>
                    <td>10 000 Kč</td>
                    <td></td>
                    <td>2 000 Kč</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="new">
                    <td colspan="14"><a href="#">Nový záznam</a></td>
                </tr>
                <tr class="pattern" style="display: none">
                    <td>skip</td>
                    <td>date</td>
                    <td>text</td>
                    <td>text</td>
                    <td>number</td>
                    <td>number</td>
                    <td>number</td>
                    <td>number</td>
                    <td>number</td>
                    <td>number</td>
                    <td>number</td>
                    <td>number</td>
                    <td>number</td>
                    <td>number</td>
                </tr>
            </tbody>
        </table>
        <div class="table-overlay">
            <button type="button" class="btn btn-sm btn-warning edit">Upravit</button>
            <button type="button" class="btn btn-sm btn-danger delete">Smazat</button>
        </div>
    </div>
</section>

<!-- Modal pro potvrzení smazání -->
<div class="modal fade" id="deleteDiaryModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Upozornění</h4>
      </div>
      <div class="modal-body">
          Opravdu chcete smazat tento záznam peněžního deníku?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Ne</button>
        <button type="button" class="btn btn-danger delete-confirm">Smazat</button>
      </div>
    </div>
  </div>
</div>
<script src="./js/denik.js"></script>