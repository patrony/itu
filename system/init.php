<?php

header("Content-Type: text/html; charset=UTF-8");

// Zapnutí session //
session_save_path(dirname(__FILE__).'/sessions');
ini_set('session.gc_maxlifetime',60*60);
ini_set('session.gc_probability',1);
ini_set('session.gc_divisor',1); 

session_start();

// Nastavení error reporting - vypnuto vše //
error_reporting(0);

// Výchozí titulek //
$title = "Úvodní strana";

// CONFIG //
$GLOBALS['config'] = array(
    // DATABÁZE //
    'mysql' => array(
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'db' => 'ITU'
    ),
    // Po jaké době neaktivity uživatele odhlásit //
    'timeout' => array(
        'time' => 3600 // 60 minut
    ),
    // SESSION //
    'session' => array(
        'session_name' => 'user',
        'token_name' => 'token'
    )
);

// AUTOLOADER TŘÍD //
spl_autoload_register(function($class){
    require_once 'system/classes/'.$class.'.php';  
});

// Funkce //
require_once 'system/functions/functions.php';