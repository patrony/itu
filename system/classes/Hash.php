<?php
/*
 * Třída pro hashování
 */
class Hash{
    /**
     * Vypočte hash SHA-256
     * @param string $string Z čeho
     * @param string $salt Sůl
     * @return string Hash o délce 64 bytů
     */
    public static function make($string, $salt = ''){
        return hash('sha256', $string . $salt);
    }

    /**
     * Vygeneruje salt
     * @param int $length Délka výsledného stringu
     * @return string
     */
    public static function salt($length){
        /*if(function_exists ( "mcrypt_create_iv" )){
            return mcrypt_create_iv($length);
        }
        $fp = fopen('/dev/urandom', 'r');
        $randomString = fread($fp, $length);
        fclose($fp);*/
        $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/\\][{}\'";:?.>,<!@#$%^&*()-_=+|';

        $randString = "";
        for ($i = 0; $i < $length; $i++) {
            $randString .= $charset[mt_rand(0, strlen($charset) - 1)];
        }

        return $randString; 
    }

    /**
     * Vygeneruje náhodný hash
     * @return string
     */
    public static function unique(){
        return self::make(uniqid());
    }
}