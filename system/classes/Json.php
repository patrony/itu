<?php
/**
 * Třída pro komunikaci s AJAXem
 */
class Json{
    // Status chyby formuláře
    const STAT_FORM_ERR = 2;
    // Status chyby
    const STAT_ERR = 3;
    // Status přesměrování
    const STAT_REDIRECT = 4;
    // Status pro přenačtení stránky
    const STAT_RELOAD = 5;
    
    /**
     * Vygeneruje JSON chybu
     * @param string $message Zpráva chyby
     */
    public static function error($message = ''){
        $send = new stdClass();
        $send->status = self::STAT_ERR;
        $send->message = $message;
        echo json_encode($send); 
    }
    
    /**
     * Vygeneruje JSON požadavek o přenačtení stránky
     */
    public static function reload(){
        $send = new stdClass();
        $send->status = self::STAT_RELOAD;
        echo json_encode($send); 
    }
    
    /**
     * Vygeneruje JSON chybu formuláře
     * @param string $form ID formuláře
     * @param string $message Zpráva chyby formuláře
     */
    public static function simple_form_error($form, $message){
        $send = new stdClass();
        $send->status = self::STAT_FORM_ERR;
        $send->form = $form;
        $send->errors = array("form" => array($message));
        echo json_encode($send); 
    }
    
    /**
     * Vygeneruje chyby položek formuláře
     * @param string $form ID formuláře
     * @param array $errors Pole chybových zpráv [JménoPrvku=>Chyba]
     */
    public static function form_errors_array($form, $errors){
        $send = new stdClass();
        $send->status = self::STAT_FORM_ERR;
        $send->form = $form;
        $send->errors = array("form" => array());
        foreach($errors as $field=>$error){
            if(is_int($field)){
                $send->errors["form"][] = $error;
            }else{
                $send->errors[$field] = $error;
            }
        }
        echo json_encode($send); 
    }
    
    /**
     * Vygeneruje přesměrování
     * @param string $link Odkaz kam přesměrovat
     */
    public static function redirect($link){
        $send = new stdClass();
        $send->status = self::STAT_REDIRECT;
        $send->link = $link;
        echo json_encode($send); 
    }
}