<?php
/**
 * Třída pro práci se vstupem
 */
class Input{
    /**
     * Zda jsou na vstupu nějaká data
     * @param string $type Typ vstupu  - post | get
     * @return boolean
     */
    public static function exists($type = 'post'){
        switch($type){
            case 'post':
                return !empty($_POST);
            case 'get':
                return !empty($_GET);
            default: 
                return false;
        }  
    }

    /**
     * Vrátí hodnotu vstupu
     * @param name $item Název vstupního pole
     * @return mixed
     */
    public static function get($item){
        if(isset($_POST[$item])){
            return $_POST[$item];
        }
        else if(isset($_GET[$item])){
            return $_GET[$item];
        }
        return NULL; 
    }
}