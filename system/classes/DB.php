<?php
/**
 * Třída pro práci s databází
 */
class DB{
    /** @var \DB Instance */
    private static $_instance = null;
    /** @var PDO PDO */
    private $_pdo;
    /** @var PDOStatement Query */
    private $_query; 
    /** @var boolean Nastala v aplikaci chyba? */
    private $_error = false;
    /** @var array Výsledky dotazu na databázi */
    private $_results;
    /** @var int Počet vrácených řádků */
    private $_count = 0;
  
    private function __construct(){
        try{
            $this->_pdo = new PDO('mysql:host=' . Config::get('mysql/host')
                    .';dbname='.Config::get('mysql/db'), 
                    Config::get('mysql/username'), Config::get('mysql/password'));
            
            $this->query("SET NAMES 'utf8'");
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }
  
    /**
     * Vytvoří instanci třídy pokud neexistuje
     * @return \DB
     */
    public static function getInstance(){
        if(!isset(self::$_instance)){
          self::$_instance = new DB();
        }
        self::$_instance->clear();
        return self::$_instance;  
    }
    
    public function clear(){
        $this->_count = 0;
        $this->_results = NULL;
        $this->_error = false;
        $this->_query = NULL;
    }


    /**
     * Provede dotaz na databázi
     * @param string $sql SQL dotaz
     * @param array $params Parametry dotazu
     * @return \DB
     */
    public function query($sql, $params = array()){
        $this->clear();
        if($this->_query = $this->_pdo->prepare($sql)){
            if(count($params)){
                $x = 1;
                foreach($params as $param){
                    $this->_query->bindValue($x,$param);
                    $x++;
                }
            }
            
            if($this->_query->execute()){
                $this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
                $this->_count = $this->_query->rowCount();
            }else{
                $this->_error = true;
            }
        }

        return $this;
    }
  
    /**
     * Sestaví dotaz na databázi, který je následně proveden
     * @param string $action SQL akce
     * @param string $table SQL tabulka
     * @param array[3] $where Podmínka [sloupec, operátor, hodnota]
     * @return self | boolean
     */
    private function action($action, $table, $where = array()){
        if(count($where) === 3){
            $operators = array('=','>','<','>=','<=','<>');

            $field    = $where[0];
            $operator = $where[1];
            $value    = $where[2];

            if(in_array($operator,$operators)){
                $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";
                if(!$this->query($sql,array($value))->error()) return $this;
            }      
        }
        return false;
    }
  
    /**
     * Načte všechna odpovídající data z tabulky
     * @param string $table Tabulka
     * @param array[3] $where Podmínka [sloupec, operátor, hodnota]
     * @return self | boolean
     */
    public function get($table, $where){
        return $this->action('SELECT *',$table, $where);  
    }
  
    /**
     * Smaže všechna odpovídající data z tabulky
     * @param string $table Tabulka
     * @param array[3] $where Podmínka [sloupec, operátor, hodnota]
     * @return self | boolean
     */
    public function delete($table, $where){
        return $this->action('DELETE',$table, $where); 
    }
  
    /**
     * Vloží data do tabulky
     * @param string $table Tabulka
     * @param array $fields Data
     * @return boolean
     */
    public function insert($table, $fields = array()){
        $keys = array_keys($fields);
        $values = null;
        $x = 1;

        foreach($fields as $field){
            $values .= '?';
            if($x < count($fields)) $values .= ', ';
            $x++;
        }

        $sql = "INSERT INTO {$table} (`".implode('`,`', $keys)."`) VALUES ({$values})";

        if(!$this->query($sql,$fields)->error()){
            return true;
        }
        return false;
    }
    
    /**
     * Vrátí ID naposledy vloženého řádku do DB
     * @return int
     */
    public function lastInsertId(){
        return $this->_pdo->lastInsertId();
    }

    /**
     * Upraví všechna odpovídající data v tabulce
     * @param string $table Tabulka
     * @param array $fields Nové hodnoty
     * @param array[3] $where Podmínka [sloupec, operátor, hodnota]
     * @return self | boolean
     */
    public function update($table, $fields = array(), $where = array()){
        $set = '';
        $x = 1;

        if(count($where) !== 3) return false;
        $operators = array('=','>','<','>=','<=','<>');

        $where_field        = $where[0];
        $where_operator     = $where[1];
        $where_value        = $where[2];

        foreach($fields as $name=>$value){
            $set .= "{$name}=?";
            if($x < count($fields)) $set .= ",";
            $x++;
        }

        $sql = "UPDATE {$table} SET {$set} WHERE {$where_field} {$where_operator} ?";

        $fields[] = $where_value;
        if(!$this->query($sql, $fields)->error()) return true;
        return false;
    }
  
    /**
     * Výsledky dotazu
     * @return array
     */
    public function results(){
        return $this->_results; 
    }
  
    /**
     * První výsledek
     * @return PDOStatement
     */
    public function first(){
        $res = $this->results();
        if(!count($res)) return NULL;
        return $res[0]; 
    }
  
    /**
     * Chyba dotazu?
     * @return array
     */
    public function error(){
        return $this->_error;
    }
  
    /**
     * Počet vrácených řádků
     * @return int
     */
    public function count(){
        return $this->_count;
    }
}