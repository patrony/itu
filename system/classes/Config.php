<?php

/** Konfigurační třída **/
class Config
{
    /**
     * Načte informace z konfigu
     * @param int $path cesta k informaci ve vnořených polích
     * @return mixed
     */
    public static function get($path = null){
        if($path){
            $config = $GLOBALS['config'];
            $path = explode('/',$path);

            foreach($path as $bit){
                if(isset($config[$bit])){
                    $config = $config[$bit];
                }
            }

            return $config;
        }
        return false;  
    }
}