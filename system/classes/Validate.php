<?php
/**
 * Třída pro validační operace, především formulářových dat
 */
class Validate
{
    /** @var boolean Je naparsováno? */
    private $_passed = false;
    
    /** @var array Chyby */
    private $_errors = array();
    
    /** @var array Prvky k validaci */
    private $_items = array();
    
    /** @var \DB Instance databáze */
    private $_db = null;

    public function __construct(){
      $this->_db = DB::getInstance();
    }

    /**
     * Přidá item pro validaci
     * @param string $name Název
     * @return \ValidateItem
     */
    public function addItem($name){
        return $this->_items[$name] = new ValidateItem($name);
    }

    /**
     * Ověří validitu vstupu
     * @param array $source Zdroj dat
     * @return \Validate
     */
    public function check($source){
        foreach($this->_items as $item){
            if(isset($source[$item->getName()])){
                $value = trim($source[$item->getName()]);
            }else{
                $value = null;
            }

            foreach($item->getRules() as $rule){

                // Nejprve kontrola, zda je povinná položka zadána //
                if($rule->getType() === 'required' && !strlen($value))
                {
                    $this->addError($item->getName(), $rule->getText());
                }
                // Pokud zadáno //
                else if(strlen($value)){
                    switch($rule->getType()){
                        // Ověří, zda jde o číslo //
                        case 'number':
                            if(ctype_digit($value) != $rule->getValue())
                                $this->addError($item->getName(), $rule->getText());
                        break;
                        
                        // Ověří, zda splňuje minimální délku //
                        case 'min':
                            if(strlen($value) < $rule->getValue())
                                $this->addError($item->getName(), $rule->getText());
                        break;
                        
                        // Ověří, zda splňuje maximální délku //
                        case 'max':
                            if(strlen($value) > $rule->getValue())
                                $this->addError($item->getName(), $rule->getText());
                        break;
                        
                        // Ověří, zda jsou hodnoty vstupů stejné //
                        case 'matches':
                            if($value != $source[$rule->getValue()])
                                $this->addError($item->getName(), $rule->getText());
                        break; 
                        
                        // Ověří, zda jsou hodnoty vstupů rozdílné //
                        case 'differs':
                            if($value == $source[$rule->getValue()])
                                $this->addError($item->getName(), $rule->getText());
                        break; 
                        
                        // Ověří, zda je hodnota unikátní ve sloupci tabulky //
                        case 'unique':
                            $unique_in = explode("/", $rule->getValue());
                            $check = $this->_db->get($unique_in[0], array($unique_in[1], '=', $value));
                            if($check->count())
                                $this->addError($item->getName(), $rule->getText());
                        break;
                        
                        // Ověří, zda hodnota v tabulce existuje //
                        case 'exists':
                            $exists_in = explode("/", $rule->getValue());
                            $check = $this->_db->get($exists_in[0], array($exists_in[1], '=', $value));
                            if(!$check->count())
                                $this->addError($item->getName(), $rule->getText());
                        break;
                        
                        // Ověří, zda je hodnota vstupu mezi povolenými hodnotami //
                        case 'allowed':
                            $not_allowed = true;
                            if(count($rule->getValue())){
                                foreach($rule->getValue() as $allowed_value){
                                    if($allowed_value == $value){
                                        $not_allowed = false;
                                        break;
                                    }
                                }
                            }
                            if($not_allowed) $this->addError($item->getName(), $rule->getText());
                        break;
                    }  
                } 
            }
        }
        if(empty($this->_errors)) $this->_passed = true;
        return $this;
    }
  
    /**
     * Zaznamená novou chybu
     * @param type $param1 Na jaké vstupní pole se chyba vztahuje (výchozí NULL)
     * @param type $param2 Text chyby
     */
    private function addError($param1, $param2 = NULL){
        $error = $param2;
        $field = $param1;
        if(is_null($param2)){
            $error = $param1;
            $field = NULL;
        }
        if(is_null($field)){
            $this->_errors[] = $error;
        }else{
            $this->_errors[$field] = $error;
        }
    }
  
    /**
     * Vrátí chyby zjištěné při validaci
     * @return array
     */
    public function errors(){
        return $this->_errors;
    }
  
    /**
     * Proběhla validace úspěšně?
     * @return boolean
     */
    public function passed(){
        return $this->_passed;
    }
}