<?php
/**
 * Třída pro práci se SESSION
 */
class Session{
    /**
     * Uloží nový session
     * @param string $name Jméno
     * @param mixed $value Hodnota
     * @return mixed
     */
    public static function put($name, $value){
        return $_SESSION[$name] = $value; 
    }

    /**
     * Zda session existuje
     * @param string $name Název session
     * @return boolean
     */
    public static function exists($name){
        return isset($_SESSION[$name]); 
    }

    /**
     * Vrátí hodnotu session
     * @param string $name Název session
     * @return mixed
     */
    public static function get($name){
        return $_SESSION[$name]; 
    }

    /**
     * Smaže session
     * @param string $name Název session
     */
    public static function delete($name){
        if(self::exists($name)) unset($_SESSION[$name]);
    }

    /**
     * Vytvoří flash zprávu / přečtě flash zprávu
     * @param string $name Název session
     * @param string $string Zpráva
     * @return string
     */
    public static function flash($name, $string = ''){
        if(self::exists($name)){
            $session = self::get($name);
            self::delete($name);
            return $session; 
        }else self::put($name,$string);
    }
    
    /**
     * Vytvoří flash zprávu / přečtě flash zprávu
     * @param string $name Název session
     * @param string $string Zpráva
     * @return string
     */
    public static function showFlash($name, $string){
        ?><div class="alert alert-<?php echo $name?>" role="alert" style="margin-top:20px"><?php echo $string?></div> <?php
    }
}