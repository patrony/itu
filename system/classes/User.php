<?php
class User{
    /** @var \DB Instance databáze */
    private $_db;
    /** @var PDOStatement Data uživatele */
    private $_data;
    /** @var string Název session */
    private $_sessionName;
    /** @var boolean Je přihlášen? */
    private $_isLoggedIn = false;
  
    public function __construct($user = null){
        $this->_db = DB::getInstance();

        // načte data z konfigu
        $this->_sessionName = Config::get("session/session_name");

        if(!$user){
            $time = Config::get("timeout/time");
            
            // Ověří, zda nebyl příliž dlouho neaktivní
            if ($time && Session::exists("activity") && (time() - Session::get("activity") > $time)) {
                $this->logout();
            }
            // Pokusí se uživatele načíst ze session
            elseif(Session::exists($this->_sessionName)){
                $user = Session::get($this->_sessionName);

                if($this->find($user)){
                    $this->_isLoggedIn = true;
                    Session::put("activity", time());
                }else{
                    // smaže session
                    $this->logout();
                }
            }
        }else{
            $this->find($user);
        }
    }
  
    /**
     * Vytvoří nového uživatele
     * @param array Pole do tabulky
     * @throws Exception
     */
    public function create($fields){  
        //$this->_db->query("SET NAMES 'utf8_bin'");
        if(!$this->_db->insert('user', $fields)){
            throw new Exception('There was a problem creating an user');
        }
    }
  
    /**
     * Upraví informace uživatele
     * @param array $fields Nové hodnoty
     * @param array[3] $where Podmínka [sloupec, operátor, hodnota]
     * @throws Exception
     */
    public function update($fields = array(),$where = array()){      
        if(empty($where) && $this->isLoggedIn())
            $where = array('id', '=', $this->data()->id);

        if(!$this->_db->update('user',$fields,$where)){
            throw new Exception('There was a problem updating an account');
        }
    }
  
    /**
     * Vyhledá uživatele v databázi
     * @param string | int $user Přihlašovací jméno nebo ID uživatele
     * @return boolean
     */
    public function find($user = null){
        if($user){
            $field = (is_numeric($user)) ? 'id' : 'username';
            $data = $this->_db->get('user', array($field, '=', $user));
            
            if($data && $data->count()){
                $this->_data = $data->first();
                return true;
            }
        }
        return false;
    }
  
    /**
     * Přihlásí uživatele
     * @param string $username Přihlašovací jméno
     * @param string $password Přihlašovací heslo
     * @param boolean $remember Zapamatovat?
     * @return boolean
     */
    public function login($username = null, $password = null){
        // Pokud jsou parametry již nastaveny
        if(!$username && !$password && $this->exists()){
            Session::put($this->_sessionName, $this->data()->id);
            return true;
        } else{
            $user = $this->find($username);

            if($user){
                if($this->data()->password === Hash::make($password, utf8_decode($this->data()->salt))){
                    Session::put($this->_sessionName, $this->data()->id);

                    Session::put("activity", time());
                    return true;
                }
            }
        }
        return false; 
    }
  
    /**
     * Odhlášení přihlášeného uživatele
     */
    public function logout(){
        Session::delete($this->_sessionName);
    }
  
    /**
     * Ověří zda jsou načtena data uživatele
     * @return booelan
     */
    public function exists(){
        return !empty($this->_data);
    }
  
    /**
     * Vrátí data uživatele
     * @return PDOStatement
     */
    public function data(){
        return $this->_data;
    }
  
    /**
     * Je přihlášen?
     * @return boolean
     */
    public function isLoggedIn(){
        return $this->_isLoggedIn;
    }
    
    /**
     * Vrátí roli (opravneni) uživatele
     * @return PDOStatement
     */
    public function role(){
        return $this->data()->role;
    }
    
    /**
     * Je superadmin?
     * @return boolean
     */
    public function isSuperadmin(){
        return $this->isLoggedIn() && $this->role() == 3;
    }
    
    /**
     * Je superadmin?
     * @return boolean
     */
    public function isAdmin(){
        return $this->isLoggedIn() && $this->role() >= 2;
    }
    
    /**
     * Je superadmin?
     * @return boolean
     */
    public function isOperator(){
        return $this->isLoggedIn() && $this->role() >= 1;
    }
}