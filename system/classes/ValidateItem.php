<?php
/**
 * Třída reprezentující skupinu pravidel pro validaci vstupu
 */
class ValidateItem
{
    /** @var array Pravidla */
    private $_rules = array();
    
    /** @var string Název */
    private $_name;

    public function __construct($name){
        $this->_name = $name;
    }

    /**
     * Vloží nové pravidlo
     * @param string $type Typ pravidla
     * @param mixed $value Hodnota k porovnání
     * @param string $text Chybový text
     * @return self
     */
    public function addRule($type, $value, $text){
        $this->_rules[$type] = new ValidateItemRule($type, $value, $text);
        return $this;
    }
    
    /**
     * Vrátí jméno itemu
     * @return string
     */
    public function getName(){
        return $this->_name;
    }
    
    /**
     * Vrátí pravidla
     * @return array
     */
    public function getRules(){
        return $this->_rules;
    }
}

/**
 * Třída reprezentující pravidlo pro validaci vstupu
 */
class ValidateItemRule
{
    /** @var string Typ */
    private $_type;
    
    /** @var mixed Hodnota */
    private $_value;
    
    /** @var string Text chyby */
    private $_text;

    public function __construct($type, $value, $text = NULL){
        $this->_type = $type;
        $this->_value = $value;
        $this->_text = $text;
    }
    
    /**
     * Vrátí typ pravidla
     * @return string
     */
    public function getType(){
        return $this->_type;
    }
    
    /**
     * Vrátí hodnotu pravidla
     * @return mixed
     */
    public function getValue(){
        return $this->_value;
    }
    
    /**
     * Vrátí chybový text pravidla, pokud nebyl zadán, vygeneruje ho
     * @return string
     */
    public function getText(){
        if(strlen($this->_text)) return $this->_text;
        
        switch($this->getType()){
            case 'required':
                return "Toto pole je povinné!";
            case 'number':
                return "Toto pole musí být číslo!";
            case 'min':
                return "Minimální délka musí být {$this->getValue()} znaků!";
            case 'max':
                return "Maximální délka musí být {$this->getValue()} znaků!";
            case 'matches':
                return "Hodnota se musí shodovat s hodnotou pole {$this->getValue()}!";
            case 'unique':
                return "Již obsazené!";
            case 'email':
                return "Špatný formát emailu!";
            case 'allowed':
                return "Toto není povolená hodnota!";
            default:
                return "Neplatný vstup!";
        }
    }
}