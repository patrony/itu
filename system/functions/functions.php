<?php

/**
 * Přemění HTML na prostý text 
 * @param string $string Text s HTML
 * @return string
 */
function escape($string){
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
}

/**
 * Převede formát data, vrací NULL v případě nevalidního data
 * @param string $date Vstupní datum
 * @param string $valid_format Formát vstupního data, výchozí j.n.Y
 * @param string $format Formát výstupního data, výchozí Y-m-d
 * @return string Formátované výstupní datum
 */
function formatDate($date, $valid_format = 'j.n.Y', $format = 'Y-m-d')
{
    $d = \DateTime::createFromFormat($valid_format, $date);
    if(!$d){
        return NULL;
    }
    return $d->format($format);
}  