/* 
 * Javascript peněžního deníku
 */

// Spustí se po načtení jQuery //
(function(){    
    // mazaný řádek //
    var $deleteRow = null;
    
    // při kiknutí na řádek tabulky s daty //
    $("#diary-table tbody").on("click", "tr[data-row-id]", function(e){
        $(this).toggleClass("open");
        $("td:first-child i",this).toggleClass("fa-plus").toggleClass("fa-minus");
    });
    
    // Upravit řádek tabulky //
    $("#diary-table tbody").on("click", ".edit", function(e){
        var $infoRow = $(this).closest("tr").addClass("hidden");
        var $activeRow = $infoRow.prev().addClass("editing");
        var $formRow = $("#diary-table tr.pattern").clone(true).removeClass("hidden").removeClass("pattern");
        
        $infoRow.before($formRow);
        
        // Sloupce řádku
        var $infos = $infoRow.find(".form-control-static:not(:first-child)");

        // Projde vstupy formuláře
        $("input, select", $formRow).each(function(index, el){
            var value = $infos.eq(index).text();
            var inputType = $(this).attr("type");
            
            if(inputType === "number")
            {   // číslo - potřeba přeformátovat
                value = value.replace(/ /g, '').replace('Kč', '').replace(',', '.');
            }
            else if(inputType === "date")
            {   // číslo - potřeba přeformátovat
                $(this).attr("type", "text").datepicker({
                    language: 'cs'
                });
            }
            
            $(this).val(value);
        });

        // nastaví tlačítka formuláře //
        $("button", $formRow).eq(0)
                .click(saveEditing)
                .text("Uložit");
        
        $("button", $formRow).eq(1)
                .click(cancelChanges);
    });
    
    // Mazání řádků tabulky //
    $("#diary-table tbody").on("click", ".delete", function(e){
        $deleteRow = $(this).closest("tr");
        
        // Zobrazí potrvzovací okno pro smazání
        $('#deleteDiaryModal').modal('show');
    });
    
    // Potvrzení smazání ve vyskakovacím okně
    $(".delete-confirm").on("click", function(e){
        // Skryje vyskakovací okno
        $('#deleteDiaryModal').modal('hide');
        
        // Smaže řádek
        $deleteRow.prev().remove();
        $deleteRow.remove();
    });
    
    // Nový řádek do tabulky //
    $("#diary-table .new a").on("click", function(e){ 
        e.preventDefault();
        
        // Počet datových řádků v tabulce
        var count = $("#diary-table tbody tr[data-row-id]").length + 1;
        
        // Vytvoří nový řádek
        var $newRow = $("<tr></tr>")
                .append("<td><i class='fa fa-minus fa-lg'></i></td>")
                .append("<td>"+count+".</td>")
                .append("<td colspan='4'><em>Nový záznam</em></td>")
                .attr("data-row-id", count)
                .addClass("editing open");
                
        var $formRow = $("#diary-table tr.pattern").clone(true).removeClass("hidden").removeClass("pattern");
        
        $(this).closest("tr").before($newRow);
        $(this).closest("tr").before($formRow);
        
        // Projde vstupy datumů ve formuláři a nastaví datepickery
        $("input[type=date]", $formRow).each(function(index, el){
            $(this).attr("type", "text").datepicker({
                language: 'cs'
            });
        });
        
        $("button", $formRow).eq(0)
                .click(createNew)
                .text("Vytvořit");
        
        $("button", $formRow).eq(1)
                .click(cancelNew);
    });
    
    // Funkce pro uložení změn na řádku //
    var saveEditing = function(){
        var $formRow = $(this).closest("tr");
        var $infoRow = $formRow.next();
        var $activeRow = $formRow.prev();

        $activeRow.removeClass("editing");
        
        saveChanges( $activeRow, $infoRow, $formRow );
    };
    
    // Funkce pro uložení nového řádku //
    var createNew = function(){
        var $formRow = $(this).closest("tr");
        var $infoRow = $(".pattern2").clone().removeClass("pattern2").removeClass("hidden");
        var $activeRow = $formRow.prev();

        $formRow.after($infoRow);
        $activeRow.removeClass("editing");
        
        $("td:last-child", $activeRow).removeAttr("colspan");
        $activeRow.append("<td></td><td></td><td></td>");
        
        saveChanges( $activeRow, $infoRow, $formRow );
    };
    
    // Změny převede z formuláře do tabuly
    var saveChanges = function($activeRow, $infoRow, $formRow){
        var $infos = $infoRow.find(".form-control-static:not(:first-child)");
        
        var $activeCols = $("td", $activeRow);
        
        var income = false;

        // Projde sloupce řádku předlohy pro zjištění typů vstupů
        $("input, select", $formRow).each(function(index, el){
            var $info = $infos.eq(index);
            var inputType = $(this).attr("type");
            var value = $(this).val();
            
            if(inputType === "number")
            {   // číslo - potřeba přeformátovat
                value = value.replace('.',',') + ' Kč';
            }
            
            switch(index){
                case 0:
                    $activeCols.eq(4).text(value);
                    break;
                case 1:
                    if(value === "Příjem") income = true;
                    break;
                case 2:
                    $activeCols.eq(2).text(value);
                    break;
                case 3:
                    $activeCols.eq(3).text(value);
                    break;
                case 4:
                    if(income){
                        var ival = "+" + value;
                        $activeCols.eq(5)
                                .removeClass("text-red")
                                .addClass("text-green");
                        $info.removeClass("text-red")
                                .addClass("text-green");
                    }
                    else{
                        var ival = "-" + value;
                        $activeCols.eq(5)
                                .removeClass("text-green")
                                .addClass("text-red");
                        $info.removeClass("text-green")
                                .addClass("text-red");
                    }
                    $activeCols.eq(5).text(ival);
                    break;
            }
            
            $info.text(value);
        });
        
        // Smaže řádek s tlačítky
        $formRow.next().removeClass("hidden");
        $activeRow.removeClass("editing");        
        $formRow.remove();
        showNotification();
    };
    
    // Funkce pro zrušení editace řádku //
    var cancelChanges = function(){
        // Vložený řádek
        var $formRow = $(this).closest("tr");
        var $activeRow = $formRow.prev();
        
        $formRow.next().removeClass("hidden");
        $activeRow.removeClass("editing");        
        $formRow.remove();
    };
    
    // Funkce pro zrušení vkládání nového řádku //
    var cancelNew = function(){
        // Vložený řádek
        var $formRow = $(this).closest("tr");
        var $activeRow = $formRow.prev();
        
        $activeRow.remove();        
        $formRow.remove();
    };
    
    // Zobrazení kalkulátoru //
    $(".pattern").on("click", ".input-group-btn", function(){
        numberInput = $(this).next();
        $("#calculatorButton").trigger("click");
    });
})(jQuery);


