/* 
 * Hlavni javascript
 */

// Spustí se po načtení jQuery //
(function(){
    // DIV s tlačítky překrývající řádek tabulky při nakliknutí //
    var $divOverlay = $('.table-overlay');
    
    // naposledy nakliknutý řádek //
    var activeRowId = 0;
    
    // při kiknutí na řádek tabulky s daty //
    $("#diary-table tbody").on("click", "tr[data-row-id]", function(e){
        // Pokud již řádek edituje, nic se nestane
        if($(this).is(".editing")) return;
        
        // Označí řádek jako aktivní 
        activeRowId = $(this).attr("data-row-id");
        
        close = false; // zabrání skrytí table-overlay
        
        // Výpočet pozice kliknutí v rámci řádku
        var posX = e.pageX - $(this).offset().left;

        var btnW = 0; // šířka obsahu table-overlay

        // Zjistí šířku tlačítek uvnitř table-overlay //
        $divOverlay.show();
        $('*', $divOverlay).each(function(){
            btnW  += $(this).outerWidth(true);
        });
        $divOverlay.hide();
        
        // Pozice a rozměry řádku 
        var rowW = $(this).css('width');
        var rowH = $(this).css('height');
        var rowPos = $(this).position();
        var rowTop = rowPos.top;
        
        // Vypočte odsazení tlačítek podle pozice kliku //
        var padding = posX - btnW/2;
        
        if(padding < 0)
        {   // pokud je za levým okrajem
            padding = 0;
        }
        else if((padding + btnW) > parseInt(rowW))
        {   // pokud je za pravým okrajem
            padding = parseInt(rowW) - btnW;
        }
        
        // nastaví rozměry a pozici table-overlay aby překrýval řádek //
        $divOverlay.css({
            position: 'absolute',
            top: rowTop,
            left: '0px',
            paddingLeft: padding + 'px',
            width: rowW,
            height: rowH
        });
        
        // Zobrazí table-overlay //
        $divOverlay.slideDown("fast", function(){
            $(".table-overlay").css('display', 'flex');
        });
    });
    
    // skryje .table-overlay //
    var close = false;
    $(".table-overlay").on("mouseleave tap", function(e){
        close = true;
        
        // časová prodleva 400 ms - kdyby omylem sjel, tak má čas se vrátit
        setTimeout(function(){
            if(close) $divOverlay.hide();
        }, 400); 
    });
    
    // přeruší skrytí table-overlay pokud na něj stihne včas najet //
    $(".table-overlay").on("mouseenter", function(e){
        close = false;
    });
    
    // Upravit řádek tabulky //
    $(".table-overlay .edit").on("click", function(e){
        // Aktivní řádek tabulky
        var $activeRow = $("#diary-table tr[data-row-id="+activeRowId+"]");
        $activeRow.addClass("editing");
        
        // Skryje table-overlay
        $divOverlay.hide();
        
        // Sloupce řádku
        var $cols = $activeRow.children("td");

        // Projde sloupce řádku předlohy pro zjištění typů vstupů
        $("#diary-table tbody tr.pattern td").each(function(index, el){
            var isDate = false;
            var inputType = $(this).text();
            
            // sloupec upravovaného řádku odpovídající aktuálně procházenému sloupci předlohy
            var $col = $cols.eq(index);
            
            var value = $col.text();
            
            if(inputType === "skip") return true;
            else if(inputType === "date")
            {   // datum
                inputType = "text";
                isDate = true;
            }
            else if(inputType === "number")
            {   // číslo - potřeba přeformátovat
                value = value.replace(/ /g, '').replace('Kč', '').replace(',', '.');
            }
            
            // vnitřní šířka sloupce = šířka inputu
            var width = $col.innerWidth() - 10;
            
            // Vytvoří nový input
            var $input = $("<input type='" + inputType + "'>")
                    .width(width)
                    .val(value)
                    .data("original", $col.text()); // původní data sloupce
            
            if(isDate)
            {   // Pokud má být datum, vytvoří datepicker
                $input.datepicker({
                    language: 'cs',
                    orientation: "auto top"
                });
            }
            
            // Obsah sloupce je input
            $col.html($input);
        });
        
        // vytvoří tlačítko pro uložení změn //
        var $saveBtn = $("<button type='button'>Uložit</button>")
                .addClass("btn btn-success btn-sm mr5")
                .click(saveChanges);
        
        // vytvoří tlačítko pro zrušení změn //
        var $cancelBtn = $("<button type='button'>Zrušit</button>")
                .addClass("btn btn-default btn-sm")
                .click(cancelChanges);
        
        // vytvoří nový řádek pro tlačítka //
        var $newCol = $("<td colspan='14'></td>").append($saveBtn)
                .append($cancelBtn);
        var $newRow = $("<tr></tr>").append($newCol);
        
        // vloží řádek s tlačítky pod editovaný řádek //
        $activeRow.after($newRow);
    });
    
    // Mazání řádků tabulky //
    $(".table-overlay .delete").on("click", function(e){ 
        $divOverlay.hide();
        
        // Zobrazí potrvzovací okno pro smazání
        $('#deleteDiaryModal').modal('show');
    });
    
    // Potvrzení smazání ve vyskakovacím okně
    $(".delete-confirm").on("click", function(e){
        // Mazaný řádek
        var $activeRow = $("#diary-table tr[data-row-id="+activeRowId+"]");
        
        // Skryje vyskakovací okno
        $('#deleteDiaryModal').modal('hide');
        
        // Smaže řádek
        $activeRow.remove();
    });
    
    // Nový řádek do tabulky //
    $("#diary-table .new a").on("click", function(e){ 
        e.preventDefault();
        
        // Počet datových řádků v tabulce
        var count = $("#diary-table tbody tr[data-row-id]").length + 1;
        
        // Vytvoří nový řádek
        var $newRow = $("<tr></tr>")
                .append("<td>"+count+".</td>")
                .attr("data-row-id", count)
                .addClass("editing");
    
        // Zobrazí řádek předlohy pro získání šířky sloupců
        $("#diary-table tbody tr.pattern").show();
        
        // Projde sloupce předlohového řádku //
        $("#diary-table tbody tr.pattern td").each(function(index, el){
            var isDate = false;
            var inputType = $(this).text();
            
            // Typ vstupu
            if(inputType === "skip") return true;
            else if(inputType === "date")
            {   // Datum
                inputType = "text";
                isDate = true;
            }
            
            // šířka sloupce = šířka inputu
            var width = $(this).innerWidth() - 10;
            
            // input sloupce
            var $input = $("<input type='" + inputType + "'>").width(width);
            
            if(isDate)
            {   // pokud má být datum, vytvoří datepicker
                $input.datepicker({language: 'cs',orientation: "auto top"});
            }
            
            // vytvoří nový sloupec a vloží ho do nového řádku
            var $newCol = $("<td></td>").append($input);
            $newRow.append($newCol);
        });
        $("#diary-table tbody tr.pattern").hide();
        
        // Tlačítko pro vytvoření řádku //
        var $saveBtn = $("<button type='button'>Vložit</button>")
                .addClass("btn btn-primary btn-sm mr5")
                .click(saveChanges);
        
        // Tlačítko pro zrušení vytváření řádku //
        var $cancelBtn = $("<button type='button'>Zrušit</button>")
                .addClass("btn btn-default btn-sm")
                .click(cancelNew);
        
        // řádek pro tlačítka vytvořit a zrušit //
        var $newCol = $("<td colspan='14'></td>").append($saveBtn)
                .append($cancelBtn);
        var $controlRow = $("<tr></tr>").append($newCol);
        
        // vloží oba řádky na konec tabulky
        $(this).closest("tr").before($newRow);
        $(this).closest("tr").before($controlRow);
    });
    
    // Funkce pro uložení změn na řádku //
    var saveChanges = function(){
        // Ukládaný řádek
        var $activeRow = $(this).closest("tr").prev();
        
        $activeRow.removeClass("editing");
        
        // Projde všechny sloupce ukládaného řádku
        $activeRow.children("td").each(function(index, el){
            if(!index) return true; // První sloupec s číslem vynéchává
            var text = $("input",this).val();
            
            if($("input",this).attr("type") == "number" && text.length)
            {   // Pro číslo přeformátuje výstup a vloží měnu (Kč)
                text = text.replace('.',',') + ' Kč';
            }
            
            $(this).text(text);
        });
        
        // Smaže řádek s tlačítky
        $(this).closest("tr").remove();
        showNotification();
    };
    
    // Funkce pro zrušení editace řádku //
    var cancelChanges = function(){
        // Upravovaný řádek
        var $activeRow = $(this).closest("tr").prev();
        
        $activeRow.removeClass("editing");
        
        // Projde všechny sloupce upravovaného řádku
        $activeRow.children("td").each(function(index, el){
            if(!index) return true; // První sloupec s číslem vynéchává
            
            // získá původní data sloupce
            var text = $("input",this).data("original");
            
            $(this).text(text);
        });
        
        // Smaže řádek s tlačítky
        $(this).closest("tr").remove();
    };
    
    // Funkce pro zrušení vkládání nového řádku //
    var cancelNew = function(){
        // Vložený řádek
        var $activeRow = $(this).closest("tr").prev();
        
        // Smaže vložený řádek a řádek s tlačítky
        $activeRow.remove();        
        $(this).closest("tr").remove();
    };
})(jQuery);


