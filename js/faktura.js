/* 
 * Javascript formuláře faktury
 */

// Spustí se po načtení jQuery //
(function(){
    // kalendář
    $(".calendar").datepicker({
        language: 'cs'
    });
    
    // Tlačítko kalkulátoru
    $(".btn-calculator").on("click", function(){
        numberInput = $(this).next();
        $("#calculatorButton").trigger("click");
    });
    
    // Tlačítko kontaktu
    $(".btn-contact").on("click", function(e){
        $('#contactModal').modal('show');
    });
})(jQuery);