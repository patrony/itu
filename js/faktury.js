/* 
 * Javascript faktur
 */

// Spustí se po načtení jQuery //
(function(){
    // mazaný řádek //
    var $deleteRow = null;
    
    // při kliknutí na řádek tabulky s daty //
    $("#factures-table tbody").on("click", "tr[data-row-id]", function(e){
        $(this).toggleClass("open");
        $("td:first-child i",this).toggleClass("fa-plus").toggleClass("fa-minus");
    });

    // Mazání řádků tabulky //
    $("#factures-table tbody").on("click", ".delete", function(e){
        $deleteRow = $(this).closest("tr");
        
        // Zobrazí potrvzovací okno pro smazání
        $('#deleteFactureModal').modal('show');
    });
    
    // Potvrzení smazání ve vyskakovacím okně
    $(".delete-confirm").on("click", function(e){
        // Skryje vyskakovací okno
        $('#deleteFactureModal').modal('hide');
        
        // Smaže řádek
        $deleteRow.prev().remove();
        $deleteRow.remove();
    });
})(jQuery);