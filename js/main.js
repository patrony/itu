/* 
 * Hlavni javascript
 */

var parseAjaxResponse = function(response){
    try{
        var response = $.parseJSON(response);
    }catch(Exception){
        return alert("Nastala neznámá chyba! \nOdpověď:\n" + response);
    }
    
    // pokud v odpovědi neobdrží status kód //
    if(typeof response.status === "undefined"){
        return alert("Nastala neznámá chyba!");
    }
    
    // status 3 = chyba //
    if(response.status === 3){
        return alert(response.message);
    }
    
    // status 4 = přesměrování //
    if(response.status === 4){
        return window.location = response.link;
    }
    
    // status 5 = refresh //
    if(response.status === 5){
        return window.location.reload();
    }
    
    // status 2 = formulářová chyba //
    if(response.status === 2){
        var errors = response.errors;
        var $form = $( '#' + response.form );
        $form.find(".alert").remove();
        $form.find(".has-error").each(function(){
            $(this).removeClass("has-error").tooltip('destroy');
        });
        for(var err in errors){
            if(err === "form"){
                var $err_obj= $("<div></div>").addClass("alert alert-warning");
                for(var fe in errors.form){
                    var $error = $err_obj.clone().html(errors.form[fe]);
                    $form.prepend($error);
                }
            }
            else{
                $form.find("[name="+err+"]").closest(".form-group")
                        .addClass("has-error")
                        .tooltip({
                            title: errors[err]
                        });
            }
        }
    }
};

// Spustí se po načtení jQuery //
(function(){
    // automaticky zobrazí nápovědu (tooltip) //
    $('body').tooltip({
        selector: '[rel=tooltip]'
    });
    
    // ajaxové odesílání formulářů - automaticky //
    $(document).on('submit', 'form', function(event){
        event.preventDefault(); // zabrání klasickému odeslání

        $.ajax({
            type: "POST",
            url: this.action,
            data: $(this).serialize(),
            success: parseAjaxResponse
        });
    });
    
    // Ajaxové odkazy //
    $(document).on("click", "a.ajax", function(event){
        event.preventDefault(); // zabrání klasickému přesměrování

        $.ajax({
            type: "POST",
            url: this.href,
            success: parseAjaxResponse
        });
    });
})(jQuery);


