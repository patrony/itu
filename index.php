<?php 
    require_once('system/init.php');
    $title = "Hlavní strana";
?>    
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Účetnictví</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="./css/font-awesome.min.css">
        <script src="./js/jquery-1.11.3.min.js"></script>
    </head>
    <body>
        <div class="wrapper">
        <!-- Navigace -->
        <?php

        // nahraje navigační lištu //
        include("includes/header.php");

        // Výchozí stránka //
        $page = "dashboard";
        ?>
        <!-- Obsah stránky -->
        <div class="content-wrapper col-md-10 pull-right" style="min-height: 200px">
            <?php

            // načte požadovanou stránku //
            if(Input::exists('get') && ($page = Input::get('s'))){
                $page = basename($page);
            }
            if(!file_exists("pages/{$page}.php")){
                include("includes/errors/404.php");
            }
            else{
                include("pages/{$page}.php");
            }
            
            ?>
        </div>
        <?php
            // Menu //
            include("includes/aside.php");
        ?>
        </div>
        <?php
            // Kalkulačka //
            include("includes/calculator.php");
        ?>
        <script src="./js/bootstrap.min.js"></script>
        <script>
        document.title = "<?php echo strip_tags ($title) ?> | " + document.title;
        </script>
    </body>
</html>