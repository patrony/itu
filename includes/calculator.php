<!-- Modal -->
<div class="modal fade" id="CalculatorModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Kalkulačka</h4>
      </div>
      <div class="modal-body" id="calculator">
          <div id="display">0</div>
          <table class="col-xs-12">
              <tr>
                  <td class="operation">√</td>
                  <td class="operation">x<sup>y</sup></td>
                  <td class="operation">%</td>
                  <td class="clear">CE</td>
              </tr>
              <tr>
                  <td class="number">7</td>
                  <td class="number">8</td>
                  <td class="number">9</td>
                  <td class="operation">÷</td>
              </tr>
              <tr>
                  <td class="number">4</td>
                  <td class="number">5</td>
                  <td class="number">6</td>
                  <td class="operation">×</td>
              </tr>
              <tr>
                  <td class="number">1</td>
                  <td class="number">2</td>
                  <td class="number">3</td>
                  <td class="operation">-</td>
              </tr>
              <tr>
                  <td class="number">0</td>
                  <td class="dot">.</td>
                  <td class="equal">=</td>
                  <td class="operation">+</td>
              </tr>
          </table>
          <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
        <button type="button" class="btn btn-primary">Vložit výsledek do pole</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(function() {
    var operation = null;
    var number1 = '0';
    var number2 = '0';
    var dot = false;
    var $display = $("#display");
    
    var pushNumber = function(number){
        if(operation === null){
            number1 += number;
        }else{
            number2 += number;
        }
    };
    
    var calculator_evaluate = function(){
        var no1 = parseFloat(number1);
        var no2 = parseFloat(number2);
        var result = 0;
        
        switch(operation){
            case '+':
                result = no1 + no2;
                break;
            case '-':
                result = no1 - no2;
                break;
            case '×':
                result = no1 * no2;
                break;
            case '÷':
                result = no1 / no2;
                break;
            case '√':
                if(no1 === 0) no1 = 1;
                result = no1 * Math.sqrt(no2);
                break;
            case '^':
                result = Math.pow(no1, no2);
                break;
            case '%':
                result = (no2 / 100) * no1;
                break;
            case null: return;
        }
        number1 = '' + result;
        number2 = '0';
        operation = null;
        dot = false;
        $display.text(number1.replace('.', ','));
    };
    
    $("#calculator .number").click(function(){
        if($display.text() === "0"){
            $display.text("");
        }
        var number = $(this).text();
        $display.append(number);
        pushNumber(number);
    });
    
    $("#calculator .dot").click(function(){
        if(dot) return;
        dot = true;
        $display.append(',');
        pushNumber('.');
    });
    
    $("#calculator .clear").click(function(){
        $display.text('0');
        dot = false;
        number1 = number2 = '0';
        operation = null;
    });
    
    $("#calculator .operation").click(function(){
        if(operation !== null){
            calculator_evaluate();
        }
        operation = $(this).text();      
        dot = false;
        if(operation == 'xy') operation = '^';
        else if(operation === '√' && $display.text() === "0"){
            $display.text("");
        }
        $display.append(operation);
    });
    
    $("#calculator .equal").click(calculator_evaluate);
    
    $('#CalculatorModal').on('hidden.bs.modal', function (e) {
        $display.text('0');
        dot = false;
        number1 = number2 = '0';
        operation = null;
    });
});
</script>