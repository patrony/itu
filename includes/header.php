<header class="header">
    <!-- Logo -->
    <a id="logo" href="index.html" class="col-xs-2">
        <span><img src="./img/logo.png"></span>
    </a>
    <nav class="navbar-static-top col-sm-10" role="navigation">
        <div class="pull-right">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#" data-toggle="modal" data-target="#CalculatorModal"><i class="fa fa-calculator"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-calendar"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-question-circle"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>