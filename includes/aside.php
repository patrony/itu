<aside class="main-menu col-md-2 col-xs-1">
    <ul>
        <li>
            <a href="?s=dashboard"<?php if($page == "dashboard"){ echo ' class="active"';} ?>>
                <i class="fa fa-line-chart"></i> <span class="hidden-sm">Přehled</span>
            </a>
        </li>
        <li>
            <a href="?s=todo"<?php if($page == "todo"){ echo ' class="active"';} ?>>
                <i class="fa fa-edit"></i> <span class="hidden-sm">TODO list</span>
            </a>
        </li>
        <li>
            <a href="?s=zakazky"<?php if($page == "zakazky"){ echo ' class="active"';} ?>>
                <i class="fa fa-clone"></i> <span class="hidden-sm">Zakázky</span>
            </a>
        </li>
        <li>
            <a href="?s=faktury"<?php if($page == "faktury"){ echo ' class="active"';} ?>>
                <i class="fa fa-archive"></i> <span class="hidden-sm">Faktury</span>
            </a>
        </li>
        <li>
            <a href="?s=denik"<?php if($page == "denik"){ echo ' class="active"';} ?>>
                <i class="fa fa-book"></i> <span class="hidden-sm">Peněžní deník</span>
            </a>
        </li>
        <li>
            <a href="?s=kontakt"<?php if($page == "kontakt"){ echo ' class="active"';} ?>>
                <i class="fa fa-users"></i> <span class="hidden-sm">Kontakty</span>
            </a>
        </li>
        <li>
            <a href="?s=mojefirma"<?php if($page == "mojefirma"){ echo ' class="active"';} ?>>
                <i class="fa fa-home"></i> <span class="hidden-sm">Moje Firma</span>
            </a>
        </li>
    </ul>
    
</aside>
<div id="menu-keeper" class="col-md-2"></div>